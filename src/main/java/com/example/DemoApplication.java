package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.GenericSelector;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.SourcePollingChannelAdapterSpec;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.core.PollerSpec;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.support.Consumer;
import org.springframework.integration.dsl.support.GenericHandler;
import org.springframework.integration.dsl.support.MessageProcessorMessageSource;
import org.springframework.integration.endpoint.MethodInvokingMessageSource;
import org.springframework.integration.handler.MessageProcessor;
import org.springframework.integration.jdbc.JdbcPollingChannelAdapter;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.PollableChannel;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@EnableIntegration
public class DemoApplication /*extends IntegrationFlowAdapte*/ {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public MessageSource<?> messageProcessorMessageSource() {
        return new MessageProcessorMessageSource(new MessageProcessor<String>() {
            @Override
            public String processMessage(Message<?> message) {
                return "";
            }
        });
    }
    @Bean
    public MessageSource<Integer> integerMessageSource() {
        MessageSource<Integer> source = new MessageSource<Integer>() {
            final AtomicInteger atomicInteger = new AtomicInteger();
            final MessageHeaders/*Map<String, Object>*/ headers = new MessageHeaders(new HashMap<>());
            @Override
            public Message<Integer> receive() {
                return getMessage(atomicInteger.incrementAndGet());
            }

            private Message<Integer> getMessage(final int number) {
                return new Message<Integer>() {
                    @Override
                    public Integer getPayload() {
                        return  number;
                    }

                    @Override
                    public MessageHeaders getHeaders() {
                        return headers;
                    }
                };
            }
        };
//        MethodInvokingMessageSource source = new MethodInvokingMessageSource();
//        source.setObject(atomicCounter());
//        source.setMethodName("getAndIncrement");
        return source;
    }
//
//    @Bean
//    private AtomicInteger atomicCounter() {
//        return new AtomicInteger() { getAndIncrement };
//    }

    @Bean(name = "helloChannel")
    public DirectChannel inputChannel() {
        return new DirectChannel();
    }

    @Bean(name = "intChannel")
    public PollableChannel inputChannel2() {
        return new QueueChannel();
    }


    @Bean(name = "outChannel")
    public PollableChannel outputChannel() {
        return new QueueChannel();
    }


    @Bean(name = "outChannel2")
    public PollableChannel outputChannel2() {
        return new QueueChannel();
    }

    @Bean
    public IntegrationFlow myFlowHW() {
        return IntegrationFlows.from(inputChannel())
                .filter("World"::equals)
                .transform("Hello "::concat)
                .channel(outputChannel())/*handle(System.out::println)*/
                .get();
    }

    @Bean
    public IntegrationFlow myFlow() {
        return IntegrationFlows.from(integerMessageSource(), getEndpointConfigurer()) /*c ->
              //  .channel(inputChannel2())
                .filter( new GenericSelector<Integer>() {
                    @Override
                    public boolean accept(Integer p) {
                        return p >= 0;
                    }
                })
                .transform(new GenericTransformer<Integer, String>() {
                    public String transform(Integer source) {
                        return source.toString();
                    }
                })
                .channel(outputChannel2(MessageChannels.queue()).handle(new GenericHandler<String>() {
                    public Object handle(String payload, Map<String, Object> headers) {
                        System.out.println(payload);
                        return payload;
                    }
                })
                */
                .channel(outputChannel2())
                .get();
    }


    @Bean(name = "FixedRate")
    public PollerMetadata poller() {
        return Pollers.fixedRate(100).maxMessagesPerPoll(1).get();
    }

    @Bean(name = "Now")
    public PollerMetadata pollerNow() {
        return Pollers.fixedDelay(0).maxMessagesPerPoll(1).get();
    }

/*    @Bean(name = "CronExpression")
    public String cron() {
        return "";
    }*/

/*    @Bean(name = "Cron")
    public PollerMetadata pollerCron() {
        return Pollers.cron(cron()).maxMessagesPerPoll(1).get();
    }*/

/*    @Bean
    public MessageSource<Object> jdbcMessageSource() {
        return new JdbcPollingChannelAdapter((DataSource) null*//*this.dataSource*//*, "SELECT * FROM foo");
    }*/

/*    @Bean
    public IntegrationFlow pollingFlow() {
        return IntegrationFlows.from(jdbcMessageSource(),
                c -> c.poller(Pollers.fixedRate(100).maxMessagesPerPoll(1)))
                .channel("furtherProcessChannel")
                .get();
    }*/



    @Bean
    public Consumer<SourcePollingChannelAdapterSpec> getEndpointConfigurer() {
        return new Consumer<SourcePollingChannelAdapterSpec>() {
            @Override
            public void accept(SourcePollingChannelAdapterSpec sourcePollingChannelAdapterSpec) {
               // Pollers.
                sourcePollingChannelAdapterSpec.poller(poller());
            }
        };
    }

}