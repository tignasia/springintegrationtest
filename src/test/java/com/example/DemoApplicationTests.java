package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
public class DemoApplicationTests {

	@Autowired
    @Qualifier("helloChannel")
	private DirectChannel inputChannel;
    @Autowired
    @Qualifier("intChannel")
    private PollableChannel inputChannel2;
    @Autowired
    @Qualifier("outChannel")
    private PollableChannel outputChannel;

    @Autowired
    @Qualifier("outChannel2")
    private PollableChannel outputChannel2;
	@Test
	public void contextLoads() {
		//MessageChannel direct = get.getBean("input", MessageChannel.class);
		Message<String> sampleMsg = MessageBuilder.withPayload("World")
				.setHeader("key", "value")
				.build();
		inputChannel.send(sampleMsg);
        System.out.println();
     //   inputChannel.send(testMessage());
        Message output = outputChannel.receive();
        System.out.println(output.getPayload());
        assertEquals((String)output.getPayload(), "Hello World");

//
//        // receive message
        Message<?> message2 = null;

        do {
            message2 = outputChannel2.receive(5000);
            System.out.println(message2.getPayload());
            assertNotNull(message2);
        } while (message2 != null);
        //FileWritingMessageHandler
        // test output message
       // assertNotNull(((Activity)message2.getPayload()).getSentTimestamp());

	}

}
